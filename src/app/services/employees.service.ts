import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class EmployeesService {
  private url = 'http://sys4.open-web.nl/employees.json';

  constructor(private http: HttpClient) {}

  all(handler: (response) => void ): void {
    this.http.get(this.url).subscribe(handler);
  }
}
