export interface Employee {
  name: string;
  role: string;
  bio: string;
  skills: string[];
  profileImage: string;
}
