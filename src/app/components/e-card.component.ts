import {Component, Input} from '@angular/core';
import {Employee} from '../models/employee.model';

@Component({
  selector: 'app-employee-card',
  templateUrl: './e-card.component.html'
})
export class ECardComponent {
  @Input() employee: Employee;

  errorHandler(event) {
    event.target.src = '/assets/logo.png';
  }
}
