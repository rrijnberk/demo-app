import {Component, OnInit} from '@angular/core';
import {EmployeesService} from './services/employees.service';
import {Employee} from './models/employee.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  employees: Employee[];
  skills: string[];
  filterSet = new Set();

  constructor(private employeeService: EmployeesService) {}

  private distillSkills = () => {
    return Array.from(
      new Set(this.employees.map(employee => employee.skills).reduce((a, b) => a.concat(b)))).sort();
  }

  private employeeResponseHandler = (response) => {
    this.employees = response.employees;
    this.skills = this.distillSkills();
  }

  ngOnInit() {
    this.employeeService.all(this.employeeResponseHandler);
  }

  toggleFilter = (skill: string) => {
    this.filterSet[this.filterSet.has(skill) ? 'delete' : 'add'](skill);
  }

  get filteredEmployees() {
    return this.filterSet.size === 0 ? this.employees : (this.employees || []).filter(employee => {
      const results = employee.skills.map(skill => this.filterSet.has(skill));
      return results.length === 0 ? false : results.length === 1 ? results[0] : results.reduce((a, b) => a || b);
    });
  }

  has(skill) {
    return this.filterSet.has(skill);
  }
}
