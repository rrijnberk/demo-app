import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {EmployeesService} from './services/employees.service';
import {HttpClientModule} from '@angular/common/http';
import {DataViewModule} from 'primeng/dataview';
import {ECardComponent} from './components/e-card.component';

@NgModule({
  declarations: [
    AppComponent,
    ECardComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    DataViewModule
  ],
  providers: [EmployeesService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
